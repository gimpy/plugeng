from packaging.version import Version
from plugeng.plugin.schema import plugin_schema, dependency_schema, provide_schema


class BasePlugin(object):
    def __init__(self, metadata,
                 metadata_schema=plugin_schema,
                 dep_schema=dependency_schema,
                 prov_schema=provide_schema):
        self.metadata_schema = metadata_schema
        self.dependency_schema = dep_schema
        self.provide_schema = prov_schema
        self.metadata_schema.validate(metadata)
        self.name = metadata['name']
        self.friendly_name = metadata['friendly_name']
        self.description = metadata['description']
        self.version = Version(metadata['version'])
        self.parent_application = metadata['parent_application']
        for i in metadata['depends_on']:
            self.dependency_schema.validate(i)
        self.depends_on = metadata['depends_on']
        self.allow_prerelease_deps = metadata.get('allow_prerelease_deps', False)
        self.provides = metadata['provides']
        for i in metadata['event_provide']:
            self.provide_schema.validate(i)
        self.event_provide = metadata['event_provide']
        self.event_listen = metadata['event_listen']
        self.enabled = True

    def setup(self):
        """Perform any special setup behaviors this
           plugin needs to function properly after init, during registration"""
        pass

    def events(self):
        """Returns a dict containing two keys:
            event_provide -> List[Dict] (list of events this plugin publishes to)
             Dict items should be {'event_name': str, 'event_schema': Schema}
            event_listen -> List[Str] (list of events this plugin listens for)"""
        return {'event_provide': self.event_provide, 'event_listen': self.event_listen}

    def handle_event(self, event_name, message):
        """Perform whatever actions are necessary based on the received event.
           Can publish other events as needed.  Only runs if plugin enabled.
            event_name -> Str
            message -> Dict (matches schema of event with included name)"""
        if not self.enabled:
            return
        self._event_handler(event_name, message)

    def _event_handler(self, event_name, message):
        """Method intended to be overridden in actual plugins.
           Perform whatever actions are necessary based on the received event.
           Can publish other events as needed.
            event_name -> Str
            message -> Dict (matches schema of event with included name)"""
        pass

    def toggle_enabled(self):
        self.enabled = not self.enabled
