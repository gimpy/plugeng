from packaging.version import Version
from schema import Schema, And, Optional
from plugeng.event.baseevent import BaseEvent

plugin_schema = Schema({
    'name': And(str, lambda s: len(s) > 2),
    'friendly_name': And(str, lambda s: len(s) > 2),
    'description': str,
    'version': And(str, lambda s: Version(s)),
    'parent_application': And(str, lambda s: len(s) > 1),
    'depends_on': list,
    Optional('allow_prerelease_deps'): bool,
    'provides': And(list, lambda l: len(l) >= 1),
    'event_provide': list,
    'event_listen': list,
    Optional(object): object,
})

dependency_schema = Schema({
    'name': And(str, lambda s: len(s) > 0),
    Optional('version'): And(str, lambda s: Version(s))
})

provide_schema = Schema({
    'event_name': str,
    'event_schema': BaseEvent
})
