import importlib
import inspect
import logging
import pkgutil
import sys

from plugeng.plugin.baseplugin import BasePlugin
from plugeng.event_manager.eventmanager import EventManager
from plugeng.plugin_manager.exceptions import CircularDependencyError


logger = logging.getLogger()


class PluginManager(object):
    def __init__(self, prefix, eventmanager=None):
        if isinstance(prefix, str) and len(prefix) > 0:
            self.prefix = prefix.strip('_') + '_'
        else:
            raise ValueError("Plugin prefix must be a string of one or more characters")
        self.plugins = []
        self.eventmanager = EventManager() if eventmanager is None else eventmanager
        self.provided_features = {}

    def register_all(self):
        """Registers all plugins with a matching prefix,
           including dependency resolution"""
        plugin_list = self.enumerate_plugins()
        if not plugin_list:
            logger.debug('No plugins found to load.')
            return
        imported_modules = {
            name: importlib.import_module(name) for name in plugin_list
        }
        unloaded_plugins = []
        for i in imported_modules:
            plugin_class = self.find_plugin_class(imported_modules[i])
            if plugin_class is not None:
                unloaded_plugins += [plugin_class]
        constructed_plugins = {}
        for i in unloaded_plugins:
            temp_plugin = self.init_plugin(i)
            constructed_plugins[temp_plugin.name] = temp_plugin
        unordered_plugins = self._collect_deps(constructed_plugins.values())
        dependency_check = self._dependencies_exist(unordered_plugins)
        if dependency_check is not True:
            for key in dependency_check:
                del unordered_plugins[key]
        if len(unordered_plugins.keys()) == 0:
            return
        if self._no_circular_dependencies(unordered_plugins) is False:
            logger.error('Circular dependency found in plugin list, halting load.  Check all plugins: {}'.format(
                unordered_plugins
            ))
            raise CircularDependencyError('Circular dependency found: {}'.format(unordered_plugins))
        load_order = self._dependency_order(unordered_plugins)
        for i in load_order:
            self.register_plugin(constructed_plugins[i])

    def _collect_deps(self, plugins):
        """Constructs a dict with a list of each plugin's dependencies"""
        out = {}
        for i in plugins:
            out[i.name] = [thing['name'] for thing in i.depends_on]
        return out

    def _dependencies_exist(self, items):
        """Confirms all direct dependencies exist"""
        all_dependencies_exist = True
        plugins_missing_deps = []
        for item, dependencies in items.items():
            for dependency in dependencies:
                if dependency not in items.keys():
                    plugins_missing_deps += [item]
                    all_dependencies_exist = False
        return all_dependencies_exist if all_dependencies_exist else plugins_missing_deps

    def _find_all_dependencies(self, all_items, item):
        """Find the full list of dependencies for an item"""
        dependencies, new_dependency_count = list(all_items[item]), -1
        while new_dependency_count != 0:
            new_dependency_count = 0
            for item in dependencies:
                new_dependencies = [new_item for new_item in all_items[item]
                                    if new_item not in dependencies]
                dependencies += new_dependencies
                new_dependency_count += len(new_dependencies)
        return dependencies

    def _no_circular_dependencies(self, items):
        """Validate that we have no circular dependencies"""
        not_circular = True
        for item in items.keys():
            dependencies = self._find_all_dependencies(items, item)
            if item in dependencies:
                not_circular = False
        return not_circular

    def _dependency_order(self, unordered_items):
        """Construct the proper order to load the plugins"""
        items, index = list(unordered_items.keys()), 0
        while (len(items) - 1) != index:
            item = items[index]
            current_dependencies = self._find_all_dependencies(unordered_items, item)
            no_order_change = True
            for dependency in current_dependencies:
                if items.index(dependency) - items.index(item) > 0:
                    items += [items.pop(index)]
                    no_order_change = False
                    break
            if no_order_change:
                index += 1
        return items

    def enumerate_plugins(self):
        """Lists the names of all plugin modules in the path"""
        plugins = []
        for _, name, _ in pkgutil.iter_modules():
            plugins += [name] if name.startswith(self.prefix) else []
        return plugins

    def find_plugin_class(self, module):
        """Finds the actual class of the plugin inside a module"""
        classes = inspect.getmembers(sys.modules[module.__name__], inspect.isclass)
        for i in classes:
            found_class = getattr(sys.modules[module.__name__], i[0])
            if found_class is BasePlugin:
                continue
            if BasePlugin in found_class.__bases__:
                return found_class
        return None

    def init_plugin(self, plugin):
        """Returns an initialized plugin"""
        return plugin()

    def register_plugin(self, plugin):
        """Registers a single plugin, ignores dependencies.  Dangerous.
           Instead of calling directly, use register_all(), which inherently
           resolves the dependency tree and makes sure everything is loaded in order."""
        plugin.setup()
        events = plugin.events()
        for i in events['event_provide']:
            self.eventmanager.register_event(i['event_name'], i['event_schema'])
        for i in events['event_listen']:
            self.eventmanager.register_listener(i, plugin.handle_event)
        plugin_classes = [i.__class__ for i in self.plugins]
        self.plugins += [plugin] if plugin.__class__ not in plugin_classes else []
        provides = plugin.provides
        for i in provides:
            pf = self.provided_features.get(i, [])
            pf += [plugin.name]
            self.provided_features[i] = pf

    def enabled_plugins(self):
        """Return a list of all enabled plugins"""
        return [i for i in self.plugins if i.enabled]

    def disabled_plugins(self):
        """Return a list of all disabled plugins"""
        return [i for i in self.plugins if not i.enabled]

    def duplicate_features(self):
        """Provides a list of all features which are duplicated
           by registered plugins, for user feedback of possible conflicts."""
        return [i for i in self.provided_features.keys() if len(self.provided_features[i]) > 1]
