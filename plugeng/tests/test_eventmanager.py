from plugeng.event_manager.eventmanager import EventManager
from plugeng.event_manager.exceptions import DuplicateEventError
from plugeng.event.baseevent import BaseEvent
from plugeng.plugin.baseplugin import BasePlugin
from plugeng.tests.fixtures import fakeplugin
from unittest.mock import Mock

import pytest


class FakeEvent(BaseEvent):
    def __init__(self):
        schema = {'message': str}
        super().__init__(schema)


class FakePlugin(BasePlugin):
    def __init__(self):
        meta = {
            'name': 'prefix_goodplugin',
            'friendly_name': 'GoodPlugin',
            'description': 'A fake, but good plugin',
            'version': '0.0.1dev1',
            'parent_application': 'TheThing',
            'depends_on': [{
                'name': 'prefix_this_other_plugin',
                'version': '1.0.0'
            }],
            'provides': ['magic'],
            'event_provide': [],
            'event_listen': ['FakeEvent', 'OtherFakeEvent']
        }
        super().__init__(meta)

    def handle_event(self, event_name, message):
        print('{}'.format(message))

    def events(self):
        return {'event_provide': self.event_provide, 'event_listen': self.event_listen}


def test_eventmanager_init():
    em = EventManager()
    assert em.events == {}
    assert em.handlers == {}


def test_eventmanager_register_good_event():
    fe = FakeEvent()
    em = EventManager()
    em.register_event('FakeEvent', fe)
    assert em.events['FakeEvent'] == fe


def test_eventmanager_register_short_name():
    fe = FakeEvent()
    em = EventManager()
    with pytest.raises(ValueError):
        em.register_event('', fe)


def test_eventmanager_register_bad_name_type():
    fe = FakeEvent()
    em = EventManager()
    with pytest.raises(TypeError):
        em.register_event({}, fe)


def test_eventmanager_register_bad_event_type():
    em = EventManager()
    with pytest.raises(TypeError):
        em.register_event('Foo', {})


def test_eventmanager_duplicate_event():
    fe = FakeEvent()
    em = EventManager()
    em.register_event('TestEvent', fe)
    with pytest.raises(DuplicateEventError):
        em.register_event('TestEvent', fe)


def test_eventmanager_register_system_event():
    fe = FakeEvent()
    em = EventManager()
    with pytest.raises(ValueError):
        em.register_event('__Foo', fe)


def test_eventmanager_register_good_listener():
    em = EventManager()
    em.register_listener('Foo', em.register_event)
    assert em.handlers['Foo'] == [em.register_event]


def test_eventmanager_register_bad_listener_name():
    em = EventManager()
    with pytest.raises(ValueError):
        em.register_listener('', em.register_event)


def test_eventmanager_register_bad_listener_name_type():
    em = EventManager()
    with pytest.raises(TypeError):
        em.register_listener(1, em.register_event)


def test_eventmanager_register_listener_bad_callable():
    em = EventManager()
    with pytest.raises(TypeError):
        em.register_listener('Foo', '1')


def test_eventmanager_event_send():
    em = EventManager()
    fp = FakePlugin()
    m = Mock()
    dl_handler = Mock()
    all_handler = Mock()
    em.register_listener('__Dead_Letter', dl_handler)
    em.register_listener('__All', all_handler)
    fp.handle_event = m
    fe = FakeEvent()
    em.register_event('FakeEvent', fe)
    em.register_event('OtherFakeEvent', fe)
    for i in fp.events()['event_listen']:
        em.register_listener(i, fp.handle_event)
    em.event('FakeEvent', {'message': 'test1'})
    em.event('OtherFakeEvent', {'message': 'test2'})
    em.event('ThirdFakeEvent', {'message': 'test3'})
    assert m.call_count == 2
    assert dl_handler.call_count == 1
    assert all_handler.call_count == 2


def test_eventmanager_event_handler_exception(capsys):
    em = EventManager()
    fp = FakePlugin()
    m = Mock()
    m.side_effect = Exception('Oh noes!')
    m.__self__ = 'test'
    m.__name__ = 'qtest'
    fp.handle_event = m
    fe = FakeEvent()
    em.register_event('FakeEvent', fe)
    em.register_event('OtherFakeEvent', fe)
    for i in fp.events()['event_listen']:
        em.register_listener(i, fp.handle_event)
    em.event('FakeEvent', {'message': 'test1'})
    em.event('OtherFakeEvent', {'message': 'test2'})
    em.event('ThirdFakeEvent', {'message': 'test3'})
    assert m.call_count == 2


def test_eventmanager_event_handler_exception_with_almost_real_plugin(capsys):
    em = EventManager()
    fp = fakeplugin.MyPlug()
    m = Mock()
    m.side_effect = Exception('Oh noes!')
    fp._event_handler = m
    fe = FakeEvent()
    em.register_event('FakeEvent', fe)
    em.register_event('OtherFakeEvent', fe)
    for i in fp.events()['event_listen']:
        em.register_listener(i, fp.handle_event)
    em.event('FakeEvent', {'message': 'test1'})
    em.event('OtherFakeEvent', {'message': 'test2'})
    em.event('ThirdFakeEvent', {'message': 'test3'})
    assert m.call_count == 1


def test_eventmanager_event_invalid_name():
    em = EventManager()
    fp = FakePlugin()
    m = Mock()
    fp.handle_event = m
    fe = FakeEvent()
    em.register_event('FakeEvent', fe)
    em.register_event('OtherFakeEvent', fe)
    for i in fp.events()['event_listen']:
        em.register_listener(i, fp.handle_event)
    with pytest.raises(ValueError):
        em.event('', {'message': 'test1'})


def test_eventmanager_event_bad_name_type():
    em = EventManager()
    fp = FakePlugin()
    m = Mock()
    fp.handle_event = m
    fe = FakeEvent()
    em.register_event('FakeEvent', fe)
    em.register_event('OtherFakeEvent', fe)
    for i in fp.events()['event_listen']:
        em.register_listener(i, fp.handle_event)
    with pytest.raises(TypeError):
        em.event(1, {'message': 'test1'})


def test_eventmanager_event_invalid_schema():
    em = EventManager()
    fp = FakePlugin()
    m = Mock()
    invalid_handler = Mock()
    em.register_listener('__Invalid_Message', invalid_handler)
    fp.handle_event = m
    fe = FakeEvent()
    em.register_event('FakeEvent', fe)
    em.register_event('OtherFakeEvent', fe)
    for i in fp.events()['event_listen']:
        em.register_listener(i, fp.handle_event)
    em.event('FakeEvent', {'message': 'test1'})
    em.event('OtherFakeEvent', {'nope': 'test2'})
    em.event('ThirdFakeEvent', {'message': 'test3'})
    assert m.call_count == 1
    assert invalid_handler.call_count == 1


def test_eventmanager_event_invalid_type():
    em = EventManager()
    fp = FakePlugin()
    m = Mock()
    fp.handle_event = m
    fe = FakeEvent()
    em.register_event('FakeEvent', fe)
    em.register_event('OtherFakeEvent', fe)
    for i in fp.events()['event_listen']:
        em.register_listener(i, fp.handle_event)
    with pytest.raises(TypeError):
        em.event('FakeEvent', 'foo')
