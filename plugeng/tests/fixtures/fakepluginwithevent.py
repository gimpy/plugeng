from plugeng.plugin.baseplugin import BasePlugin
from plugeng.event.baseevent import BaseEvent


class MyPlug(BasePlugin):
    """Fake Plugin to test with"""
    def __init__(self):
        meta = {
            'name': 'prefix_myplug',
            'friendly_name': 'MyPlug',
            'description': 'A fake, but good plugin',
            'version': '0.0.1dev1',
            'parent_application': 'TheThing',
            'depends_on': [],
            'provides': ['magic'],
            'event_provide': [{'event_name': 'ThisSpecialEvent', 'event_schema': BaseEvent({'message': str})}],
            'event_listen': ['FakeEvent'],
        }
        super().__init__(meta)
