from plugeng.plugin.baseplugin import BasePlugin


class prefix_plug7(BasePlugin):
    """Fake Plugin to test with"""
    def __init__(self):
        meta = {
            'name': 'prefix_plug7',
            'friendly_name': 'Plug7',
            'description': 'A fake, but good plugin',
            'version': '0.0.1dev1',
            'parent_application': 'TheThing',
            'depends_on': [],
            'provides': ['magic'],
            'event_provide': [],
            'event_listen': ['FakeEvent'],
        }
        super().__init__(meta)
