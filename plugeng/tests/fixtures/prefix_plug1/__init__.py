from plugeng.plugin.baseplugin import BasePlugin


class prefix_plug1(BasePlugin):
    """Fake Plugin to test with"""
    def __init__(self):
        meta = {
            'name': 'prefix_plug1',
            'friendly_name': 'Plug1',
            'description': 'A fake, but good plugin',
            'version': '0.0.1dev1',
            'parent_application': 'TheThing',
            'depends_on': [{'name': 'prefix_plug2'}, {'name': 'prefix_plug3'}, {'name': 'prefix_plug5'}],
            'provides': ['magic'],
            'event_provide': [],
            'event_listen': ['FakeEvent'],
        }
        super().__init__(meta)
