from plugeng.plugin.baseplugin import BasePlugin
from plugeng.event.baseevent import BaseEvent
from packaging.version import Version
from schema import SchemaError, Schema
from unittest.mock import Mock

import pytest


goodevent = BaseEvent(Schema({'message': str}))

goodmeta = {
    'name': 'prefix_goodplugin',
    'friendly_name': 'GoodPlugin',
    'description': 'A fake, but good plugin',
    'version': '0.0.1dev1',
    'parent_application': 'TheThing',
    'depends_on': [{
        'name': 'prefix_this_other_plugin',
        'version': '1.0.0'
    }],
    'provides': ['magic'],
    'event_provide': [{'event_name': 'fakeevent', 'event_schema': goodevent}],
    'event_listen': [],
}

extrameta = {
    'name': 'prefix_goodplugin',
    'friendly_name': 'GoodPlugin',
    'description': 'A fake, but good plugin',
    'version': '0.0.1dev1',
    'parent_application': 'TheThing',
    'depends_on': [{
        'name': 'prefix_this_other_plugin',
        'version': '1.0.0'
    }],
    'provides': ['magic'],
    'event_provide': [{'event_name': 'fakeevent', 'event_schema': goodevent}],
    'event_listen': [],
    'optional1': 'hello1',
    'optional2': 'hello2',
    'optional3': 'hello3',
}

badmeta = {
    'name': 'p',
    'friendly_name': 'G',
    'description': 'A fake, but bad plugin',
    'version': 'hey_la',
    'parent_application': 'T',
    'depends_on': {},
    'provides': None
}


def test_baseplugin_goodmeta():
    plugin = BasePlugin(goodmeta)
    assert plugin.name == 'prefix_goodplugin'
    assert plugin.friendly_name == 'GoodPlugin'
    assert plugin.description == 'A fake, but good plugin'
    assert plugin.version == Version('0.0.1dev1')
    assert plugin.parent_application == 'TheThing'
    assert plugin.depends_on == [{'name': 'prefix_this_other_plugin', 'version': '1.0.0'}]
    assert plugin.provides == ['magic']


def test_baseplugin_extrameta():
    plugin = BasePlugin(extrameta)
    assert plugin.name == 'prefix_goodplugin'
    assert plugin.friendly_name == 'GoodPlugin'
    assert plugin.description == 'A fake, but good plugin'
    assert plugin.version == Version('0.0.1dev1')
    assert plugin.parent_application == 'TheThing'
    assert plugin.depends_on == [{'name': 'prefix_this_other_plugin', 'version': '1.0.0'}]
    assert plugin.provides == ['magic']


def test_baseplugin_badmeta():
    with pytest.raises(SchemaError):
        plugin = BasePlugin(badmeta)
        print(plugin)


def test_baseplugin_setup():
    plugin = BasePlugin(goodmeta)
    plugin.setup()


def test_baseplugin_events():
    plugin = BasePlugin(goodmeta)
    assert plugin.events()['event_provide'][0]['event_name'] == 'fakeevent'
    assert plugin.events()['event_listen'] == []


def test_baseplugin_handle_event():
    plugin = BasePlugin(goodmeta)
    m = Mock()
    plugin._event_handler = m
    plugin.handle_event('foo', {})
    assert m.call_count == 1


def test_baseplugin_handle_event_disabled():
    plugin = BasePlugin(goodmeta)
    m = Mock()
    plugin._event_handler = m
    plugin.toggle_enabled()
    plugin.handle_event('foo', {})
    assert m.call_count == 0


def test_baseplugin_toggle_enabled():
    plugin = BasePlugin(goodmeta)
    assert plugin.enabled is True
    plugin.toggle_enabled()
    assert plugin.enabled is False
    plugin.toggle_enabled()
    assert plugin.enabled is True
