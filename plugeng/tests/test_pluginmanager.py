from plugeng.event.baseevent import BaseEvent
from plugeng.plugin_manager.pluginmanager import PluginManager
from plugeng.plugin_manager.exceptions import CircularDependencyError
from plugeng.tests.fixtures import fakeplugin
from plugeng.tests.fixtures import fakepluginwithevent
from unittest.mock import patch

import importlib
import pathlib
import pkgutil
import pytest
import sys
import os


class MiniPlugin(object):
    def __init__(self, name, depends_on):
        self.name = name
        self.depends_on = []
        for i in depends_on:
            self.depends_on += [{'name': i}]


def test_pluginmanager_empty_prefix():
    with pytest.raises(ValueError):
        PluginManager('')


def test_pluginmanager_bad_prefix():
    with pytest.raises(ValueError):
        PluginManager({})


def test_pluginmanager_good_prefix():
    pm = PluginManager('prefix')
    assert pm.prefix == 'prefix_'


def test_pluginmanager_good_prefix_with_underscore():
    pm = PluginManager('prefix_')
    assert pm.prefix == 'prefix_'


def test_pluginmanager_find_plugin_class():
    pm = PluginManager('prefix')
    assert pm.find_plugin_class(fakeplugin) == fakeplugin.MyPlug


def test_pluginmanager_find_no_plugin():
    pm = PluginManager('prefix')
    assert pm.find_plugin_class(pytest) is None


def test_pluginmanager_init_plugin():
    pm = PluginManager('prefix')
    assert pm.init_plugin(pm.find_plugin_class(fakeplugin)).name == 'prefix_myplug'


def test_pluginmanager_register_plugin():
    pm = PluginManager('prefix')
    pm.register_plugin(pm.init_plugin(pm.find_plugin_class(fakeplugin)))
    assert len(pm.plugins) == 1
    assert 'magic' in pm.provided_features
    assert pm.provided_features['magic'] == ['prefix_myplug']


def test_pluginmanager_register_event_plugin():
    pm = PluginManager('prefix')
    pm.register_plugin(pm.init_plugin(pm.find_plugin_class(fakepluginwithevent)))
    assert len(pm.plugins) == 1
    assert 'magic' in pm.provided_features
    assert pm.provided_features['magic'] == ['prefix_myplug']


def test_pluginmanager_enabled_plugins_with_one():
    pm = PluginManager('prefix')
    pm.register_plugin(pm.init_plugin(pm.find_plugin_class(fakeplugin)))
    assert len(pm.enabled_plugins()) == 1


def test_pluginmanager_enabled_plugins_with_zero():
    pm = PluginManager('prefix')
    pm.register_plugin(pm.init_plugin(pm.find_plugin_class(fakeplugin)))
    pm.plugins[0].toggle_enabled()
    assert len(pm.enabled_plugins()) == 0


def test_pluginmanager_disabled_plugins_with_one():
    pm = PluginManager('prefix')
    pm.register_plugin(pm.init_plugin(pm.find_plugin_class(fakeplugin)))
    pm.plugins[0].toggle_enabled()
    assert len(pm.disabled_plugins()) == 1


def test_pluginmanager_disabled_plugins_with_zero():
    pm = PluginManager('prefix')
    pm.register_plugin(pm.init_plugin(pm.find_plugin_class(fakeplugin)))
    assert len(pm.disabled_plugins()) == 0


def test_pluginmanager_reregister_no_dupes():
    pm = PluginManager('prefix')
    pm.register_plugin(pm.init_plugin(pm.find_plugin_class(fakeplugin)))
    assert len(pm.plugins) == 1
    pm.register_plugin(pm.init_plugin(pm.find_plugin_class(fakeplugin)))
    assert len(pm.plugins) == 1


def test_pluginmanager_enumerate_plugins():
    pm = PluginManager('prefix')
    fakemodules = ['prefix_foo', 'prefix_bar', 'notprefix_baz']
    override_modules = []
    for i in pkgutil.iter_modules():
        override_modules += [i]
    for i in fakemodules:
        override_modules += [('', i, '')]
    print(pm.enumerate_plugins())
    with patch('pkgutil.iter_modules', return_value=override_modules):
        assert pm.enumerate_plugins() == ['prefix_foo', 'prefix_bar']


def test_pluginmanager_event():
    class FakeEvent(BaseEvent):
        def __init__(self):
            schema = {'message': str}
            super().__init__(schema)
    fe = FakeEvent()
    pm = PluginManager('prefix')
    pm.register_plugin(pm.init_plugin(pm.find_plugin_class(fakeplugin)))
    pm.eventmanager.register_event('FakeEvent', fe)
    pm.eventmanager.event('FakeEvent', {'message': 'test'})


def test_pluginmanager_collect_deps_no_plugins():
    pm = PluginManager('prefix')
    assert pm._collect_deps([]) == {}


def test_pluginmanager_collect_deps_one_plugin():
    p1 = MiniPlugin('p1', ['p2', 'p3'])
    pm = PluginManager('prefix')
    assert pm._collect_deps([p1]) == {'p1': ['p2', 'p3']}


def test_pluginmanager_collect_deps_multi_plugin():
    p1 = MiniPlugin('p1', ['p2', 'p3'])
    p2 = MiniPlugin('p2', ['p3'])
    p3 = MiniPlugin('p3', [])
    pm = PluginManager('prefix')
    assert pm._collect_deps([p1, p2, p3]) == {'p1': ['p2', 'p3'], 'p2': ['p3'], 'p3': []}


def test_pluginmanager_dependencies_exist_true():
    p1 = MiniPlugin('p1', ['p2', 'p3'])
    p2 = MiniPlugin('p2', ['p3'])
    p3 = MiniPlugin('p3', [])
    pm = PluginManager('prefix')
    assert pm._dependencies_exist(pm._collect_deps([p1, p2, p3])) is True


def test_pluginmanager_dependencies_exist_missing():
    p1 = MiniPlugin('p1', ['p2', 'p3'])
    p2 = MiniPlugin('p2', ['p3'])
    p3 = MiniPlugin('p3', ['p4'])
    pm = PluginManager('prefix')
    assert pm._dependencies_exist(pm._collect_deps([p1, p2, p3])) == ['p3']


def test_pluginmanager_find_all_dependencies_no_dependencies():
    p1 = MiniPlugin('p1', [])
    pm = PluginManager('prefix')
    assert pm._find_all_dependencies(pm._collect_deps([p1]), 'p1') == []


def test_pluginmanager_find_all_dependencies_many_dependencies():
    p1 = MiniPlugin('p1', ['p2', 'p3'])
    p2 = MiniPlugin('p2', ['p3'])
    p3 = MiniPlugin('p3', ['p4'])
    p4 = MiniPlugin('p4', [])
    pm = PluginManager('prefix')
    assert pm._find_all_dependencies(pm._collect_deps([p1, p2, p3, p4]), 'p1') == ['p2', 'p3', 'p4']


def test_pluginmanager_no_circular_dependencies_true():
    p1 = MiniPlugin('p1', ['p2', 'p3'])
    p2 = MiniPlugin('p2', ['p3'])
    p3 = MiniPlugin('p3', ['p4'])
    p4 = MiniPlugin('p4', [])
    pm = PluginManager('prefix')
    assert pm._no_circular_dependencies(pm._collect_deps([p1, p2, p3, p4])) is True


def test_pluginmanager_no_circular_dependencies_false():
    p1 = MiniPlugin('p1', ['p2', 'p3'])
    p2 = MiniPlugin('p2', ['p3'])
    p3 = MiniPlugin('p3', ['p4'])
    p4 = MiniPlugin('p4', ['p1'])
    pm = PluginManager('prefix')
    assert pm._no_circular_dependencies(pm._collect_deps([p1, p2, p3, p4])) is False


def test_pluginmanager_dependency_order():
    p1 = MiniPlugin('p1', ['p5'])
    p2 = MiniPlugin('p2', ['p1'])
    p3 = MiniPlugin('p3', ['p1'])
    p4 = MiniPlugin('p4', ['p3'])
    p5 = MiniPlugin('p5', [])
    pm = PluginManager('prefix')
    dep_order = pm._dependency_order(pm._collect_deps([p1, p2, p3, p4, p5]))
    assert dep_order.index('p5') == 0
    assert dep_order.index('p1') < dep_order.index('p2')
    assert dep_order.index('p1') < dep_order.index('p3')
    assert dep_order.index('p4') > dep_order.index('p3')


def test_pluginmanager_dependency_order_complex():
    p1 = MiniPlugin('p1', ['p2', 'p3', 'p5'])
    p2 = MiniPlugin('p2', ['p4'])
    p3 = MiniPlugin('p3', ['p6'])
    p4 = MiniPlugin('p4', ['p3'])
    p5 = MiniPlugin('p5', ['p7'])
    p6 = MiniPlugin('p6', [])
    p7 = MiniPlugin('p7', [])
    pm = PluginManager('prefix')
    deps = pm._collect_deps([p1, p2, p3, p4, p5, p6, p7])
    dep_order = pm._dependency_order(deps)
    assert dep_order.index('p6') < dep_order.index('p3')
    assert dep_order.index('p7') < dep_order.index('p5')
    assert dep_order.index('p5') < dep_order.index('p1')
    assert dep_order.index('p4') > dep_order.index('p3')
    assert dep_order.index('p3') < dep_order.index('p1')
    assert dep_order.index('p2') < dep_order.index('p1')


def test_pluginmanager_duplicate_features():
    cwd = os.getcwd()
    path = pathlib.Path(cwd + '/plugeng/tests/fixtures')
    sys.path += [str(path)]
    importlib.invalidate_caches()
    pm = PluginManager('prefix')
    pm.register_all()
    assert len(pm.plugins) == 7
    assert pm.duplicate_features() == ['magic']


def test_pluginmanager_register_all_happy_path():
    cwd = os.getcwd()
    path = pathlib.Path(cwd + '/plugeng/tests/fixtures')
    sys.path += [str(path)]
    importlib.invalidate_caches()
    pm = PluginManager('prefix')
    pm.register_all()
    assert len(pm.plugins) == 7


def test_pluginmanager_register_all_no_plugins():
    pm = PluginManager('prefix2')
    pm.register_all()
    assert len(pm.plugins) == 0


def test_pluginmanager_register_all_missing_deps():
    cwd = os.getcwd()
    path = pathlib.Path(cwd + '/plugeng/tests/fixtures')
    sys.path += [str(path)]
    importlib.invalidate_caches()
    pm = PluginManager('prefix3')
    pm.register_all()
    assert len(pm.plugins) == 0


def test_pluginmanager_register_all_circular_deps():
    cwd = os.getcwd()
    path = pathlib.Path(cwd + '/plugeng/tests/fixtures')
    sys.path += [str(path)]
    importlib.invalidate_caches()
    pm = PluginManager('circular')
    with pytest.raises(CircularDependencyError):
        pm.register_all()
    assert len(pm.plugins) == 0
