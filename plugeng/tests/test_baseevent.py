from plugeng.event.baseevent import BaseEvent
from schema import Schema, And, SchemaError

import pytest

testschema = Schema({
    'name': And(str, lambda s: len(s) > 0),
    'message': And(str, lambda s: len(s) > 0)
})

schemadict = {'name': str}


def test_baseevent_bad_schema_type():
    with pytest.raises(SchemaError):
        BaseEvent('foo')


def test_baseevent_dictschema():
    BaseEvent(schemadict)


def test_baseevent_empty_dictschema():
    BaseEvent({})


def test_baseevent_goodmessage():
    event = BaseEvent(testschema)
    event.validate({'name': 'RealEvent', 'message': 'Yup'})


def test_baseevent_badmessage():
    event = BaseEvent(testschema)
    assert event.validate({}) is False
