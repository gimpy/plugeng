from plugeng.event.baseevent import BaseEvent
from plugeng.event_manager.exceptions import DuplicateEventError

import copy
import logging

logger = logging.getLogger()


class EventManager(object):
    def __init__(self):
        self.events = {}
        self.handlers = {}

    def register_event(self, event_name, event):
        """Takes in an event name in the form of a str, and event object
           adds event to the event list if not already present."""
        if not isinstance(event_name, str):
            raise TypeError("Event names must be strings, got: {}".format(type(event_name)))
        if len(event_name) < 1:
            raise ValueError("Event names must be at least length 1")
        if event_name.startswith('__'):
            raise ValueError("Event names beginning with __ are system reserved.")
        if not isinstance(event, BaseEvent):
            raise TypeError("Event object must be a child of BaseEvent, got: {}".format(type(event)))
        if event_name in self.events.keys():
            raise DuplicateEventError("Event '{}' already exists".format(event_name))
        self.events[event_name] = event

    def register_listener(self, event_name, handler):
        """Takes in an event name in the form of a str, and the event_handler
           method of a plugin."""
        if not isinstance(event_name, str):
            raise TypeError("Event names must be strings, got: {}".format(type(event_name)))
        if len(event_name) < 1:
            raise ValueError("Event names must be at least length 1")
        if not callable(handler):
            raise TypeError("Handler must be a callable, got: {}".format(type(handler)))
        handlers = self.handlers.get(event_name, [])
        handlers += [handler]
        self.handlers[event_name] = handlers

    def event(self, event_name, message):
        """Takes in an event, validates message against the schema,
           then publishes to all listeners.  Event names which do not exist
           are published to __Dead_Letter.  Messages with invalid schemas
           are published to __Invalid_Message.  Every message additionally
           gets published to __All."""
        if not isinstance(event_name, str):
            raise TypeError("Event names must be strings, got: {}".format(type(event_name)))
        if len(event_name) < 1:
            raise ValueError("Event names must be at least length 1")
        if not isinstance(message, dict):
            raise TypeError("Messages must be dictionaries, got: {}".format(type(message)))
        mut_message = copy.deepcopy(message)
        mut_message['OriginalEvent'] = event_name
        if event_name not in self.events:
            # Publish messages for events which have not been registered to __Dead_Letter for debugging.
            logger.warning('Received event {}, which is unregistered.  Publishing to __Dead_Letter.'.format(event_name))
            self.__event_publish('__Dead_Letter', mut_message)
            return
        event = self.events[event_name]
        if not event.validate(message):
            # Publish invalid messages to __Invalid_Message for debugging.
            logger.warning('Received message for event "{}", which did not match schema: {}'.format(
                event_name, message
            ))
            self.__event_publish('__Invalid_Message', mut_message)
            return
        # Publish EVERY valid message to the __All listeners.  Intended for logging/debug.
        self.__event_publish('__All', mut_message)
        # Finally, publish to the actual listeners for this event.
        self.__event_publish(event_name, message)

    def __event_publish(self, event_name, message):
        """Internal message publisher"""
        for handler in self.handlers.get(event_name, []):
            try:
                handler(event_name, message)
            except Exception as e:
                logger.warning('{}.{} raised exception {} when calling handler.'.format(
                    handler.__self__,
                    handler.__name__,
                    e))
