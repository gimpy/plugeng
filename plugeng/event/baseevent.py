from schema import SchemaError, Schema


class BaseEvent(object):
    def __init__(self, schema):
        """Takes in a Schema object or dict representation"""
        if isinstance(schema, dict):
            self.schema = Schema(schema)
            return
        if not isinstance(schema, Schema):
            raise SchemaError("Events must be provided a schema")
        self.schema = schema

    def validate(self, message):
        """Validates that the message matches our schema
           Returns: bool"""
        try:
            self.schema.validate(message)
        except SchemaError:
            return False
        return True
