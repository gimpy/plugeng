from plugeng import __version__

from setuptools import setup, find_packages

setup(
    name='plugeng',
    version=__version__,
    description='Python plugin engine with event based communication',
    author='Jayme Howard',
    author_email='g.prime@gmail.com',
    license='MIT',
    keywords='plugin event',
    project_urls={
        'Source Code': 'https://gitlab.com/gimpy/plugeng'
    },
    packages=find_packages(
        exclude=[
            '*.tests',
            '*.tests.*',
            'tests.*',
            'tests',
            'log',
            'log.*',
            '*.log',
            '*.log.*'
        ]
    ),
    python_requires='>=3.5',
    install_requires=[
        'packaging~=19.0',
        'schema~=0.7.0',
    ]
)
