# Design for a Python-based Plugin System with Event-Based Communication

Probably outdated by now, but I kinda updated it.  Sort of.  Maybe.


## Plugin

Plugin objects are the base of the system and are core functionality.
All plugins should be defined as singletons (?)
A plugin's init function should set up the singleton functionality (?) and the metadata for the plugin.
Minimum required metadata:
* name -> Str (Plugin name, same as pip package name)
* friendly_name -> Str (User displayed name)
* description -> Str (Plugin description)
* version -> Str (Plugin version, PEP-440 format)
* parent_application -> Str (Name of application this plugin is built for)
* depends_on -> List[Str] (List of other plugins, version optional but in PEP-440 format, this plugin depends on)
* allow_prerelease_deps -> Optional(Bool) (Whether we allow prerelease versions in the dependency tree for this plugin)
* provides -> List[Str] (List of one-word features this plugin provides, ex "logging", "DiscordPresence", etc)


## PluginManager

PluginManager is a singleton which handles discovery and registration of all plugins.
The init function should take in a string to be used as a prefix to discover installed plugins.
The register_all function should iterate through all matching plugins with the prefix, import them, and capture their metadata.  After capturing all metadata, resolve dependency tree and register plugins in order.
If any dependencies are unavailable, log an error and skip registration of that plugin.


## Event

Event objects are defined as Schema.
Event objects have a validate function which takes in the passed message, and returns a boolean value indicating success or failure to validate.


## EventManager

EventManager is an object which should have events and listeners registered to it.
Registering a listener for an event does not require the event to already exist.
